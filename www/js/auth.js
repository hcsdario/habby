/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var apiUrl="http://habbyonline.it/script/api.php";
var auth = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
        var self = this;

        $("#signup").click(function() {
                if($(".formMessage.error").length)
                {
                    alert("Devi controllare che l'username e l'email inserite siano valide");
                    return;
                }
                var email = $("#formSignin #email").val();
                if(
                    $("#fullname").val()==""||
                    $("#name").val()==""||
                    $("#formSignin #password").val()==""||
                    email==""||
                    !email.includes(".")||
                    !email.includes("@")
                ){
                    alert("Uno dei campi inseriti non è stato compilato o l'email inserita non è valida. Si prega di ricontrollare i datu inseriti");
                    return;
                }
                var user=$("#formSignin #user").val();

                $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: main.formToData("#formSignin"),
                    crossDomain: true,
                    cache: false,
                    beforeSend: function() {
                        $("#signup").val('Connecting...');
                    },
                    success: function(data) {
                        var kind=0;
                        var message=data.message;
                        if (message == "success") {
                          var userid=data.id;
                            localStorage.userid=userid;
                            localStorage.isLogged=1;
                            self.getInitData(kind);
                        } else if (message == "exist") {
                            alert("L'email appartiene ad un account attivo, non è possibile concludere la registrazione. ");
                        } else if (message == "failed") {
                            alert("Si è verificato un errore durante la registrazione");
                        }
                    }
                });

        });

        $("#login").click(function() {
                if($("#formLogin #user").val()==""||$("#formLogin #password").val()=="")
                    return;
                var user=$("#formLogin #user").val();
                var dataApi = main.formToData("#formLogin");
                $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: dataApi,
                    crossDomain: true,
                    cache: false,
                    beforeSend: function() {
                        $("#login").html('Connecting...');
                    },
                    success: function(data) {
                        if (data.message == "success") {
                            localStorage.login = "true";
                            localStorage.user = user;
                            localStorage.userid = data.id;
                            self.getInitData(data.kind);
                        } else
                            alert(data.text);

                    },
                    complete: function(r){
                        $("#login").html('Login'); 
                    }

                });
        });

        if(window.location.search!=""&&window.location.search.includes("logout"))
            localStorage.clear();

    },

  
    onDeviceReady: function () {
            alert("onDeviceReady");
            window.plugins.uniqueDeviceID.get(success, fail);

            function success(uuid)
            {
            debugger;
                alert(uuid);
                $("deviceIdContainer").text(uuid);
            };
            
            function fail(uuid)
            {
            debugger;
                alert(uuid);
            };
    },

    getInitData: function (kind){
            if(kind==0){

                $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: {
                        action:'getDashboardData',
                        user: localStorage['user'] 
                    },
                    kind:kind,
                    crossDomain: true,
                    cache: false,
                    success: function(data) {
                        localStorage['dashboardData'] = JSON.stringify(data);        
                        window.location.href = "dashboard.html";
                    }
                });
            }else{

                $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: {
                        action:'getTecDashboardData',
                        user: localStorage['user'] 
                    },
                    kind:kind,
                    crossDomain: true,
                    cache: false,
                    success: function(data) {
                        localStorage['tecDashboardData'] = JSON.stringify(data);  
                        window.location.href = "tecdashboard.html";
                    }
                });        
            }
            
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'auth.receivedEvent(...);'
    onDeviceReady: function() {
        auth.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
    }
};
