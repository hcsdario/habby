/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var apiUrl="http://habbyonline.it/script/api.php";
var main = {

    // Application Constructor
    initialize: function() {
        var self = this;


        /* navigation */
        $("#goeShop").click(function() {
            window.location.href = "https://habbyonline.it/";
        });
        //B&B
        //https://habbyonline.it/servizi/servhotel/
        /*
        $("#goeService").click(function() {
            window.location.href = "https://habbyonline.it/";
        });
        */
        $("#goSettings").click(function() {
            window.location.href = "settings.html";
        });
        $("#goPoint").click(function() {
            window.location.href = "https://habbyonline.it/dove-siamo/";
        });

        $("#goBlog").click(function() {
            window.location.href = "https://habbyonline.it/blog/";
        });


        $("#goSite").click(function() {
            window.location.href = "https://habbyonline.it/";
        });

        $("#goLogout").click(function() {
            window.location.href = "index.html?action=logout";
        });


        /* script per la pagina del profilo: al click estraggo i dati del profilo, e al caricamento della pagina valorizzo i campi già popolati con quelli precedentemente estratti */
        $("#goProfilo").click(function() {
            if(localStorage['profileData'] == null){
                $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: {
                        action:'getProfileData',
                        userid: localStorage['userid'] 
                    },
                    crossDomain: true,
                    cache: false,
                    success: function(data) {
                        window.location.href = "profilo.html";
                        localStorage['profileData'] = JSON.stringify(data.data);          
                    }
                });
            }else{
                window.location.href = "profilo.html";
            }

        });

        if(window.location.href.includes("profilo.html")){
            self.setProfileData();
        }
        if(window.location.href.includes("settings.html")){
            var data=JSON.parse(localStorage.profileData)
            $("#id").val(data.id);
        }

        /* all'aggiornamento del profilo salvo i dati con questa chiamata Ajax */
        $("#updateAccount").click(function() {
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: self.formToData(".form"),
                crossDomain: true,
                cache: false,
                beforeSend: function() {
                    $("#updateAccount").val('Saving...');
                },
                success: function(data) {
                    if (data && data.message == "success") {
                        alert("Hai aggiornato i tuoi dati con successo");
                        window.location.href = "history.html";
                    }
                    else {
                        alert("Si è verificato un errore durante l'aggiornamento: "+ data.message);
                    }               }
            });
        });
        /*fine script per il caricamento del profilo */

        $("#vede_solo_la_sua_zona").change(function() {
            if($(this).prop("checked"))
                $(this).val(1);
            else
                $(this).val(0);
        })
        /* aggiornamento delle preferenze di impostazioni del tecnico (salvate su user perchè siamo pigri) */
        $("#updateSettings").click(function() {
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: self.formToData("#settings"),
                crossDomain: true,
                cache: false,
                beforeSend: function() {
                    $("#updateSettings").val('Saving...');
                },
                success: function(data) {
                    if (data && data.message == "success") {
                        alert("Hai aggiornato i tuoi dati con successo");
                    }
                    else {
                        alert("Si è verificato un errore durante l'aggiornamento: "+ data.message);
                    }               
                }
            });
        });
        /*fine script per l'aggiornamento delle impostazioni */

        /* script per la pagina del dashboard: al click estraggo alcuni dati del profilo, e al caricamento della pagina valorizzo i campi  citttà, indirizzo, telefono 
        con quelli dell'utente corrente precedentemente estratti */
        $("#goDashboard").click(function() {
            window.location.href = "dashboard.html";
        });
        $("#gotecDashboard").click(function() {
            window.location.href = "tecdashboard.html";
        });

        if(window.location.href.includes("dashboard.html")){
            self.setDashbordData();
        }


        $("#sendRequest").click(function() {
            var validator = $( "#myform" ).validate();
            validator.form();       
            window['tmp']=$(this);
            if(!validator.valid()){
                return;
            }
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: self.formToData(".form"),
                crossDomain: true,
                cache: false,
                beforeSend: function() {
                    $("#sendRequest").val('Sending...');
                },
                success: function(data) {
                        alert("Hai inviato la richiesta con successo");
                    //    localStorage.removeItem('dashboardData');
                        localStorage.removeItem('historyData');
                        $("#goHistory").click();
                },error:function(e){
                    alert("Si è verificato un errore durante l'invio: "+ e.message);
                }
            });
        });

        /* script per la pagina della history: al click estraggo i dati da db, e al caricamento della pagina appendo i dati */
        $("#goHistory").click(function() {
            
            if(localStorage['historyData'] == null)
                self.getHistoryData();
            else
                window.location.href = "history.html";
            

        });
        $(".emo").click(function(){
            $(".emoContainer .emo").each(function(){
                var src =$(this).attr("src");
                    src=src.replace("Active","");
                $(this).attr("src",src);
            });
            var src =$(this).attr("src");
            $(this).attr("src",src+"Active");
            $("#stato").val($(this).attr("stato"));
        })

        $('#statusReq').change(function(){
            var value=$(this).val()
            if(value=="parziale")
                $("#noteProblem").show();
            else
                $("#noteProblem").hide();
        })

        if(window.location.href.includes("tecdashboard.html")){
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: {
                    action:'getTecDashboardData',
                    user: localStorage['user'] 
                },
                crossDomain: true,
                cache: false,
                success: function(data) {
                    localStorage['tecDashboardData'] = JSON.stringify(data);          
                    self.setTecDashbordData();
                    /* invio di una richiesta, al salvataggio verranno generate le notifiche ai tecnici competenti tramite trigger sql*/
                    $(".referRequest").click(function() {
                        window['tmp']=$(this);
                        var data=$(this).data();
                        self.referRequest (data.idrichiesta,localStorage['user']);
                    });
                    $(".deleteReferent").click(function() {
                        window['tmp']=$(this);
                        var data=$(this).data();
                        self.referRequest (data.idrichiesta);
                    });
                    $(".requestDetails").click(function() {
                        self.DetailsToModal($(this));
                    });
                    $(".closeRequest").click(function() {
                        self.closeRequestToModal($(this));
                    });
                }
            });  
        }

        if(window.location.href.includes("history.html")){
            self.appendHistoryData();
        }

//        this.bindEvents();
    },


        /* Funzioni utili */

        /* controllo che lo username o l'email non sia già presente a db: scatta in fase di registrazione e in fase di aggiornamento profilo */
        checkUsername: function (userEl, currentId) { 
            var userEl=$(userEl);
            var checkField=$(userEl).attr('name');
            var username = userEl.val();

            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: {
                    action:'checkUsername',
                    field: checkField, 
                    fieldVal: username, 
                    id: currentId
                },
                crossDomain: true,
                cache: false,
                success: function(data) {
                    if (data && data.message == "success") {
                        $(".formMessage").remove();
                        userEl.after("<p class='validated formMessage'> Valido</p>");
                    }
                    else {
                        $(".formMessage").remove();
                        userEl.after("<p class='error formMessage'>"+data.text+"</p>");
                    }
                }
            });
        },


        // jquery ajax get data from form
        formToData: function (target="form") { 
            var request={};
            $(
                target+" input[type='hidden'], "+
                target+" input[type='text'], "+
                target+" input[type='email'], "+
                target+" input[type='number'], "+
                target+" input[type='password'], "+
                target+" input[type='radio']:checked, "+
                target+" select,"+target+" textarea" 
            ).each(function(){
                var key=$(this).attr("name");
                var value=$(this).val();
                request[key]=value;
            })
            return request;
        },

        datavalidateFormField: function (data,target="") { 
            var keys=Object.keys(data);
            for(var i=0;i<keys.length;i++){
                $(target+" [name='"+keys[i]+"']").val(data[keys[i]]);
            }
        },



         /* Funzioni solo in fase di sviluppo*/
        fakedata: function () { 
            $("input[name!='action'][type!='email']").each(function(){
                var text=$(this).val();
                $(this).val(text+"1");
            })
        },



        setProfileData: function () { 
            if(localStorage['profileData']){
                var data= JSON.parse(localStorage['profileData']);
                this.datavalidateFormField(data);
            }
        },

        set_settings: function () { 

        },

        setDashbordData: function () { 
            if(localStorage['dashboardData']){
                var data= JSON.parse(localStorage['dashboardData']);
                this.datavalidateFormField(data.dataUser);
            
                var keys=Object.keys(data.settori);
                for(var i=0;i<keys.length;i++){
                    var opt="<option value='"+keys[i]+"'>"+data.settori[keys[i]]+"</option>";
                    $("#settore").append(opt);
                }

                var keys=Object.keys(data.aree);
                for(var i=0;i<keys.length;i++){
                    var opt="<option value='"+keys[i]+"'>"+data.aree[keys[i]]+"</option>";
                    $("#area").append(opt);
                }
            }
        },


        setTecDashbordData: function () { 

            if(localStorage['tecDashboardData']){
                var data= JSON.parse(localStorage['tecDashboardData']);
                var data=data.tecDashboardData;
                for(var i=0;i<data.length;i++){

                    if(data[i].assign_to_me!=0){

                        var reportNote="";
                        if(data[i].reportCliente!=null || data[i].reportTecnico!=null ){
                            if(data[i].reportCliente==null)
                                reportNote="<p>In attesa del report del cliente per chiudere questa richiesta</p>";
                            if(data[i].reportTecnico==null)
                                reportNote="<p>Devi compilare il report di questa richiesta per chiuderla definitivamente</p>";
                        }

                        var el='<li class ="accept">'+
                        '       <data>'+data[i].timer+'</data>'+
                        '        <p>Settore:<settore>'+data[i].settore+'</settore></p>'+
                        '        <p>Area:<area>'+data[i].area+'</area></p>'+
                        '        <p>'+
                        '             Richiesta: <testo>'+data[i].descrizione+'</testo>'+
                        '        </p>    '+
                        '       <stato><button type="button" class="btn btn-success" disabled>Incarico preso</button></stato>   '+
                        '        <button type="button" class="btn btn-info requestDetails" data-idrichiesta="'+data[i].id+'">Visualizza dettagli <li class="ion-ios-search-strong" data-pack="ios" data-tags="find, seek, look, magnifying glass" style="display: inline-block;"></li></button>'+
                        '        <button type="button" class="btn btn-danger deleteReferent" data-idrichiesta="'+data[i].id+'">Lascia incarico <li class="ion-ios-trash" data-pack="ios" data-tags="delete, remove, dispose, waste, basket, dump, kill" style="display: inline-block;"></li></button>'+
                        '        <button type="button" class="btn btn-warning closeRequest" data-idrichiesta="'+data[i].id+'">Richiesta Completata </button>'+
                        reportNote+
                        '   </li>';
                    }
                    else{
                        var el='<li class ="open">'+
                        '       <data>'+data[i].timer+'</data>'+
                        '        <p>Settore:<settore>'+data[i].settore+'</settore></p>'+
                        '        <p>Area:<area>'+data[i].area+'</area></p>'+
                        '        <p>'+
                        '             Richiesta: <testo>'+data[i].descrizione+'</testo>'+
                        '        </p>    '+
                        '       <stato><button type="button" class="btn btn-basic"  disabled>Da assegnare</button></stato>  '+
                        '        <button type="button" class="btn btn-info requestDetails" data-idrichiesta="'+data[i].id+'">Visualizza dettagli <li class="ion-ios-search-strong" data-pack="ios" data-tags="find, seek, look, magnifying glass" style="display: inline-block;"></li></button>'+
                        '       <button type="button" class="btn btn-warning  referRequest" data-idrichiesta="'+data[i].id+'">Prendi in carico</button>'+
                        '   </li>';
                    }

                    $("#divHistory").append(el);
                }
            }
        },

        DetailsToModal: function (el) { 
            var data= $(el).data();
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: {
                    action:'getRequestDetails',
                    richiesta_id: data.idrichiesta 
                },
                crossDomain: true,
                cache: false,
                success: function(data) {
                        var el='<p><strong>Richiesto Da </strong><utente>'+data.data.name+' '+data.data.surname+'</utente></p>'+
                        '<p>'+
                        '<strong>Recapiti Richiedente:</strong></p><p> <strong>Email </strong>'+ data.data.email +'</p><p> <strong>Cellulare: </strong>' +data.data.phone+'</p><p> <strong>Indirizzo: </strong>' +data.data.address +
                        '</p>';
                        $("#modalInfoR .modal-body").html(el);
                        $("#modalInfoR").modal();
                        //$("#modalDettagli").modal();
                                        
                }
            })                
        },
        // CloseRichiestaToModal con stato lavoro report tecnico
        closeRequestToModal: function (el) { 
            var data= $(el).data();
            /*Il lavoro è stato concluso, oppure no (perchè magari bisogna ordinare un pezzo e ci deve ritornare...)
            Se no, spiegare perché?
            Se si, se è stato eseguito con successo
            Durata dell'intervento
            Costo dell'intervento*/

            $("#modalDettagli [name='richiesta_id']").val(data.idrichiesta);
            $("#modalDettagli [name='userid']").val(localStorage.userid);
            $("#modalDettagli").modal();
            var self = this;
            $(".closeRequestConfirm").click(function() {
                
//            $("#clienteChiudeRichiesta").modal();
                self.closeRequest($(this),self.formToData("#tecReport"),true);
            });
                              
        },

        closeRequest: function (el, dataJson,refresh) {
            var data= $(el).data();
            window['tmp']=el;
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data: dataJson,
                crossDomain: true,
                cache: false,
                success: function(data) {  
                    $(window['tmp']).parents(".modal").modal("hide");
                    alert("La richiesta è stata archiviata");
                    if(refresh){
                        window.location.reload(true);
                    }
                    
                }
            })
        },

        /* invio della presa in carico di una richiesta*/
        referRequest: function (richiesta_id,userid=null) { 
            $.ajax({
                type: "POST",
                url: apiUrl,
                "dataType": "json",
                data:  {
                    action:'referRequest',
                    user: userid, 
                    richiesta_id: richiesta_id
                },
                crossDomain: true,
                cache: false,
                beforeSend: function() {
                    $("#referRequest").val('Sending...');
                },
                success: function(data) {
                    if (data && data.message == "success" && data.text != 'Eliminato') {
                        alert("Hai preso in carico la richiesta con successo, i dettagli del contatto sono: indirizzo:"+ data.data.address + " telefono: "+ data.data.phone +" email:" + data.data.email);
                    }else if(data && data.message == "success"  && data.text == 'Eliminato'){
                         alert("Hai preso in carico la richiesta con successo");
                    }
                    window.location.reload(true);
                },error:function(e){
                    alert("Si è verificato un errore durante l'invio: "+ e.message);
                }

            });
        },

        getHistoryData: function () { 
            $.ajax({
                    type: "POST",
                    url: apiUrl,
                    "dataType": "json",
                    data: {
                        action:'getHistoryData',
                        user: localStorage['user'] 
                    },
                    crossDomain: true,
                    cache: false,
                    success: function(data) {
                        localStorage['historyData'] = JSON.stringify(data.data);          
                        window.location.href = "history.html";
                    }
                });
        },


        appendHistoryData: function () { 
            var data= JSON.parse(localStorage['historyData']);
            for(var i=0;i<data.length;i++){

                if(data[i].tecnico!=null){

                    var reportNote="";
                    if(data[i].reportCliente!=null || data[i].reportTecnico!=null ){
                        if(data[i].reportTecnico==null)
                            reportNote="<p>In attesa del report del tecnico per chiudere questa richiesta</p>";
                        if(data[i].reportCliente==null)
                            reportNote="<p>Devi compilare il report di questa richiesta per chiuderla definitivamente</p>";
                    }

                    var el='<li class ="accept">'+
                    '       <data>'+data[i].timer+'</data>'+
                    '       <stato><button type="button" class="btn btn-primary" disabled>Accolta</button></stato> '+
                    '       <p>Settore:<settore>'+data[i].settore+'</settore></p>'+
                    '       <p>Area:<area>'+data[i].area+'</area></p>'+
                    '       <p>Presa in carico da:<referente>'+data[i].tecnico+'</referente></p>'+
                    '       <p>'+
                    '           Richiesta: <testo>'+data[i].descrizione+'</testo>'+
                    '       </p>    '+
                    '       <button type="button" class="btn btn-warning closeRequest" data-action="setCloseRequest" data-userid="'+localStorage.userid+'" data-idrichiesta="'+data[i].id+'" data-richiesta_id="'+data[i].id+'" >Archivia Richiesta </button>'+
                    reportNote+
                    '   </li>';
                }
                else{
                    var el='<li class ="open">'+
                    '       <data>'+data[i].timer+'</data>'+
                    '       <stato><button type="button" class="btn btn-basic"  disabled>Aperta</button></stato>    '+
                    '       <p>Settore:<settore>'+data[i].settore+'</settore></p>'+
                    '       <p>Area:<area>'+data[i].area+'</area></p>'+
                    '       <p>'+
                    '           Richiesta: <testo>'+data[i].descrizione+'</testo>'+
                    '       </p>    '+
                    '       <button type="button" class="btn btn-warning closeRequest" data-action="setCloseRequest" data-userid="'+localStorage.userid+'" data-idrichiesta="'+data[i].id+'" data-richiesta_id="'+data[i].id+'" >Archivia Richiesta </button>'+
                    '   </li>';
                }

                $("#divHistory").append(el);
                var self = this;

                $(".closeRequest:last").click(function() {
                    $("#clienteChiudeRichiesta").modal();
                    var data=$(this).data();
                    $('[name="id_richiesta"]').val(data.richiesta_id);
                });

            }

                $("#saveRCl").click(function(){
                    var request={};
                    var target="#rcl";
                    $(
                        target+" input[type='hidden'], "+
                        target+" input[type='text'], "+
                        target+" input[type='email'], "+
                        target+" input[type='number'], "+
                        target+" input[type='password'], "+
                        target+" input[type='radio']:checked, "+
                        target+" select,"+target+" textarea" 
                    ).each(function(){
                        var key=$(this).attr("name");
                        var value=$(this).val();
                        request[key]=value;
                    })

                    var stato=$("#stato").val();
                    $.ajax({
                            type: "POST",
                            url: apiUrl,
                            data: {
                                user: localStorage['user'] ,
                                stato: stato,
                                id_richiesta: request.id_richiesta,
                                action: request.action,
                                noteCliente: request.noteCliente
                            },
                            crossDomain: true,
                            cache: false,
                            success: function(data) {
                                window.location.reload(true);
                            }
                        });

                })


        },

    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'main.receivedEvent(...);'
    onDeviceReady: function() {
        main.receivedEvent('deviceready');
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
    }
};
